// webpack.config.js
const webpack = require('webpack');
const path = require('path');
var libraryName = 'akomando';

const config = {
   context: path.resolve(__dirname, 'src/api'),
   entry: './akomando.js',
   output: {
      //path: path.resolve(__dirname, 'dist/browser'),
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.akomando.js',
      library: libraryName,
      libraryTarget: 'umd',
      umdNamedDefine: true
   },
   module: {
      rules: [{
         test: /\.js$/,
         include: path.resolve(__dirname, 'src'),
         use: [{
            loader: 'babel-loader',
            options: {
               presets: [
                  ['es2015', { modules: false }]
               ]
            }
         }]
      }]
   }
};

module.exports = config;
