#!/usr/bin/env node
// libs
import program from 'commander';
import * as fs from 'fs';
import chalk from 'chalk';
import { pd } from 'pretty-data2';


// api
import { createAkomando } from '../api/akomando.js';


/**
 * Check if a path refers to a file
 * @param {String} file The path to check
 * @return {Boolean} True if the path refers to a file
*/
const _isFile = function(file){
   try {
      fs.accessSync(file, fs.F_OK);
      const stats = fs.statSync(file);
      if (stats.isFile()) {
         return true;
      }
      else {
         return false;
      }
   }
   catch (e) {
      console.log(chalk.red.bold.underline('\nThe path you inserted does not refer to a file. If you are drunk or stunned please consider to have another beer before trying again.\n'));
      console.log(e);
      return false;
   }
};

/**
 * Utility function that check the common CLI parameters and, if they're fine, creates an Akomando object
 * @return {Object} An Akomando object (or null if parameters are bad)
 */
const _openAkomando = function(file, options) {
    if (! _isFile(file))
        return null;

    const aknStream = fs.readFileSync(file);
    const aknString = aknStream.toString();

    let config;
    if(options.config){
      if(_isFile(options.config)){
         const configStream = fs.readFileSync(options.config);
         config = configStream.toString();
      }
      else{
         return null;
      }
    }

    return createAkomando({
        aknString,
        config,
    });
}

/**
 * The function that is called when the 'akomando get-doc-type <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.config=null] a configuration file according to {@link akomando.config}
 * @return {String} The doc type of the AkomaNtoso file
*/
const _getDocType = function(file,options){
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null) {
      const result = akomantoso.getDocType();
      console.log(chalk.green.bold(result));
      return result;
   }
};

/**
 * The function that is called when the 'akomando get-meta <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.serialize=AKNString] the serialization of the results
 * @param {Object} [options.html=true] false when, if requesting an HTML serialization, the html element is not required
 * @param {Object} [options.head=true] false when, if requesting an HTML serialization, the head element is not required
 * @param {Object} [options.body=true] false when, if requesting an HTML serialization, the body element is not required
 * @param {Object} [options.newLines=true] false when new lines are not requested in the results (useful when asking for textual version of the document)
 * @return {String} The AkomaNtoso document in the requested serialization
*/
const _getAkomaNtoso = function(file, options){
   let serialize = '';
   if(!options.serialize){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'json'){
      serialize = 'JSON';
   }
   else if(options.serialize === 'html'){
      serialize = 'HTMLString';
   }
   else if(options.serialize === 'akn'){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'text'){
      serialize = 'TEXT';
   }
   else{
      console.log(chalk.red.bold.underline('\nThe serialization that you requested is not supported. Supported serialization are akn, html, json. \nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }

   let akomantoso = _openAkomando(file, options);
   if (akomantoso != null) {
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();
      const addHtmlElement = options.html;
      const addHeadElement = options.head;
      const addBodyElement = options.body;
      const newLines = options.newLines;

      const result = akomantoso.getAkomaNtoso({
         serialize,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
         newLines,
      });
      let prettyPrintedResults = '';
      if(serialize === 'JSON'){
         prettyPrintedResults = pd.json(result);
      }
      else if (serialize === 'HTMLString' || serialize == 'AKNString'){
         prettyPrintedResults = pd.xml(result.toString());
      }
      else if (serialize === 'TEXT'){
         prettyPrintedResults = result.toString();
      }
      console.log(chalk.green.bold(prettyPrintedResults));
      return prettyPrintedResults;
   }
};

/**
 * The function that is called when the 'akomando get-meta <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @throws {Error} Throws error when the confing does not refer to a file
 * @throws {Error} Throws error when the serialization is not supported
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.serialize=AKNString] the serialization of the results
 * @param {Object} [options.dataXpath=true] when true, it inserts the xpath to eleents contained in the results
 * @param {Object} [options.html=true] false when, if requesting an HTML serializatiom, the html element is not required
 * @param {Object} [options.head=true] false when, if requesting an HTML serializatiom, the head element is not required
 * @param {Object} [options.body=true] false when, if requesting an HTML serializatiom, the body element is not required
 * @return {String} The meta of the AkomaNtoso file
*/
const _getMeta = function(file, options){
   let serialize = '';
   if(!options.serialize){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'json'){
      serialize = 'JSON';
   }
   else if(options.serialize === 'html'){
      serialize = 'HTMLString';
   }
   else if(options.serialize === 'akn'){
      serialize = 'AKNString';
   }
   else{
      console.log(chalk.red.bold.underline('\nThe serialization that you requested is not supported. Supported serialization are akn, html, json. \nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   let akomantoso = _openAkomando(file, options);
   if (akomantoso != null) {
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();
      const addHtmlElement = options.html;
      const addHeadElement = options.head;
      const addBodyElement = options.body;
      const insertDataXPath = options.dataXpath;

      const result = akomantoso.getMeta({
         serialize,
         metaRoot: options.metaRoot,
         insertDataXPath,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      let prettyPrintedResults = '';
      if(serialize === 'JSON'){
         prettyPrintedResults = pd.json(result);
      }
      else if (serialize === 'AKNString' || serialize == 'HTMLString'){
         prettyPrintedResults = pd.xml(result.toString());
      }
      console.log(chalk.green.bold(prettyPrintedResults));
      return prettyPrintedResults;
   }
};

/**
 * The function that is called when the 'akomando get-components <file>' command is launched
 * @throws {Error} Throws error when the path does not refer to a file
 * @throws {Error} Throws error when the confing does not refer to a file
 * @throws {Error} Throws error when the serialization is not supported
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} options The options requested by the user
 * @param {Object} [options.serialize=AKNString] the serialization of the results
 * @param {String} [options.detailedInfo=true] - If it is set to false, the function returns only an array containing the components in the requested serialization
 * @param {Object} [options.dataXpath=true] when true, it inserts the xpath to elements contained in the results
 * @param {Object} [options.html=true] false when, if requesting an HTML serializatiom, the html element is not required (if the HTML serialization is required)
 * @param {Object} [options.head=true] false when, if requesting an HTML serializatiom, the head element is not required (if the HTML serialization is required)
 * @param {Object} [options.body=true] false when, if requesting an HTML serializatiom, the body element is not required (if the HTML serialization is required)
 * @return {Array} An array that contains the information about the components contained in the document. The array contains an object structured as {@link akomando.components.getComponents}
*/
const _getComponents = function(file, options){
   let serialize = '';
   if(!options.serialize){
      serialize = 'AKNString';
   }
   else if(options.serialize === 'json'){
      serialize = 'JSON';
   }
   else if(options.serialize === 'html'){
      serialize = 'HTMLString';
   }
   else if(options.serialize === 'akn'){
      serialize = 'AKNString';
   }
   else{
      console.log(chalk.red.bold.underline('\nThe serialization that you requested is not supported. Supported serialization are akn, html, json. \nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   let akomantoso = _openAkomando(file, options);
   if (akomantoso != null) {
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();
      const addHtmlElement = options.html;
      const addHeadElement = options.head;
      const addBodyElement = options.body;
      const insertDataXPath = options.dataXpath;
      const detailedInfo = options.detailedInfo;

      let result = akomantoso.getComponents({
         serialize,
         detailedInfo,
         insertDataXPath,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      if(!detailedInfo){
         result = result[0].components;
      }

      let prettyPrintedResults = '';
      if(serialize === 'JSON'){
         prettyPrintedResults = pd.json(result);
      }
      else if (serialize === 'AKNString' || serialize == 'HTMLString'){
         (detailedInfo) ?
            prettyPrintedResults = JSON.stringify(result) :
            prettyPrintedResults = result.map( doc => (
               pd.xml(doc)
            ));
      }
      console.log(chalk.green.bold(prettyPrintedResults));
      return prettyPrintedResults;
   }
};

/**
 * Returns all ids of all hierarchies of the loaded AkomaNtoso document
 * @protected
 * @throws {Error} Throws error when the path does not refer to a file
 * @throws {Error} Throws error when the configuration does not refer to a file
 * @throws {Error} Throws error when the requested sorting is not supported
 * @throws {Error} Throws error when the requested ordering is not supported
 * @throws {Error} Throws error when filterByContent parameter is not a Boolean
 * @param {String} file The path of the AkomaNtoso file
 * @param {Object} [options] The options for resulting {@link akomando.hier.identifiers} object
 * @param {String} [options.sortyBy=position] The field on wich results must be sorted. Possible values are:
 *
 * - position - The position of the element insiede the document
 * - name - The name of the element
 * - eId - The eId of the element
 * - wId - The wId of the element
 * - GUID - The guid of the element
 * - xpath - The xpath of the element
 * - level - The level of the element
 *
 * @param {String} [options.order=ascending] The ordering of the returnet elements. Possible values are:
 *
 * - ascending - Results are ordered in ascending way
 * - descending - Results are ordered in descending way
 *
 * @param {String} [options.filterByName=null] Returns only the hierarchies having the given name
 * @param {Boolean} [options.filterByContent=false] Returns only the hierarchies having containing text (the last level partitions in hierarchies)
 * @return {Object} an {@link akomando.hier.identifier} object containing all the identifiers of all the partitions
*/
const _getHierIdentifiers = function(file, options){
   let sortBy = '';
   let order = '';
   let filterByContent = false;
   if(!options.sortBy){
      sortBy = 'position';
   }
   else if(options.sortBy != 'position' &&
      options.sortBy != 'position' &&
      options.sortBy != 'name' &&
      options.sortBy != 'eId' &&
      options.sortBy != 'wId' &&
      options.sortBy != 'GUID' &&
      options.sortBy != 'xpath' &&
      options.sortBy != 'level'){
      console.log(chalk.red.bold.underline('\nThe sorting that you requested is not supported. Supported sorting are position, name, eId, wId, GUID, xpath, level.\nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   else{
      sortBy = options.sortBy;
   }
   if(!options.order){
      order = 'ascending';
   }
   else if(options.order != 'ascending' &&
      options.order != 'descending'){
      console.log(chalk.red.bold.underline('\nThe ordering that you requested is not supported. Supported ordering are ascending and descending.\nIf you are drunk or stunned please consider to have another beer before trying again.\n'));
   }
   else{
      order = options.order;
   }
   if(options.filterByContent){
      filterByContent = true;
   }
   let akomantoso = _openAkomando(file, options);
   if (akomantoso != null) {
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();

      const result = akomantoso.getHierIdentifiers({
         sortBy,
         order,
         filterByName: options.filterByName,
         filterByContent,
      });
      console.log(chalk.green.bold(pd.json(result)));
      return pd.json(result);
   }
};

/**
 * Returns the number of elements contained in the AkomaNtoso document as a {@link akomando.elements.count} Object
 * @throws {Error} Throws error when the AkomaNtoso document is not set
 * @throws {Error} Throws error when filterByName parameter is not a String
 * @param {String} file The path of the Akoma Ntoso file in which execute the command
 * @param {Object} options The options for restricting the number of elements that must be counted
 * @param {String} [options.filterByName=null] Counts only the elements having the given name
 * @return {Object} The number of elements contained in the AkomaNtoso document as a {@link akomando.elements.count} Object
*/
const _countDocElements = function(file, options){
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null) {
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();

      const result = akomantoso.countDocElements({
         filterByName: options.filterByName,
      });
      console.log(chalk.green.bold(pd.json(result)));
      return pd.json(result);
   }
};


/**
 * Returns a list of the references that are contained in the metadata section of the document.
 * It also returns information about the references and information about the elements
 * of the document that are linked to them.
 * Results is a JSON structured as a {@link akomando.find.references}
 * @protected
 * @throws {Error} Throws error when the AkomaNtoso document is not set
 * @throws {Error} Throws error when filterBy parameter is not a String
 * @throws {Error} Throws error when the requested filtering is not supported
 * @param {String} file The path of the Akoma Ntoso file in which execute the command
 * @param {Object} [options] The options for resulting {@link akomando.find.references} object
 * @param {String} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
 *
 * - all - returns all the references
 * - used - returns only the references that are used by at least an element in the document
 * - unused - returns only the references that are not used by any element in the document
 * @return {Object} The list of elements that have a "source" attribute as
 * a {@link akomando.checks.references} Object
*/
const _getReferencesInfo = function(file, options){
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null) {
      const aknStream = fs.readFileSync(file);
      const aknString = aknStream.toString();

      const result = akomantoso.getReferencesInfo({
         filterBy: options.filterBy,
      });
      console.log(chalk.green.bold(pd.json(result)));
      return pd.json(result);
   }
};

/**
 * Returns a list of elements that refers to some Id
 * @param {String} file The path of the Akoma Ntoso file in which execute the command
 * @param {Object} [options] The options for the function
 * @param {Object} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
 * - all - returns all the references
 * - existing - returns only the references to an existing id (in the same document)
 * - pending - returns only the references to an non-exisitng id (in the same document)
 * @return {Object} A list of references, of type {@link akomando.references.idReference}
*/
const _getIdReferences = function(file, options) {
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null){
        const result = akomantoso.getIdReferences({
            filterBy: options.filterBy,
        });
        console.log(chalk.green.bold(pd.json(result)));
        return pd.json(result);
   }
};

/**
 * Returns an object containing the URIs of Work, Expression and Manifestation of the document
 * @return {Object} An object with three fields: workUri, expressionUri and manifestationUri
 */
const _getFRBR = function(file, options) {
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null){
        const result = akomantoso.getFRBR();
        console.log(chalk.green.bold(pd.json(result)));
        return pd.json(result);
   }
};

/**
 * Search elements in the document using the given XPath
 * @throws {Error} Throws error when xpath parameter is not a String
 * @param {String} file The path of the Akoma Ntoso file in which execute the command
 * @param {String} xpath The string representing the XPath
 * @param {Object} [options] The options for resulting object
 * @return {Object} A list containing all elements represented by the XPath
 */
const _getElementsFromXPath = function(file, xpath, options){
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null) {
      const result = akomantoso.getElementsFromXPath(xpath);
      console.log(chalk.green.bold(pd.xml(result.toString())));
      return pd.xml(result.toString());
   }
};

/**
 * Returns author and publication/creation date information about Manifestation or Expression or Work of the document
 * @param {String} file The path of the Akoma Ntoso file in which execute the command
 * @param {Object} [options] The options for resulting object
 * @param {String} [options.about = expression] Specifies what results we can receive. Possible values are:
 *
 * - work - returns work information
 * - expression - returns expression information
 * - manifestation - returns manifestation information
 *
 * @return {Object} An object with two fields: author, date
 */
const _getPublicationInfo = function(file, options){
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null) {
        if(options.about == 'work' || options.about == 'expression' || options.about == 'manifestation') {
            const result = pd.json(JSON.stringify(akomantoso.getPublicationInfo({ about: options.about })));
            console.log(result);
            return result;
        }
        else
            console.log(`Invalid option 'about' -${options.about}- : it must be 'work', 'expression' or 'manifestation'`);
   }
};


/**
 * Check whether the ids in the document are well-formed
 * @param {String} file The path of the Akoma Ntoso file in which execute the command
 * @param {Object} [options] The options for the function call
 * @param {String} [options.idName = all] The ids to be checked. Possible values are: 'all' (default), 'wId', 'eId'
 * @return {Object} A list of {@link akomando.document.idError} representing bad ID values
 */
const _CheckIds = function(file, options) {
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null) {
        const errList = akomantoso.checkIds({ idName: options.idName });
        const retString = errList.map((idErr) => idErr.message).join('\n');
        console.log(retString);
        return retString;
    }
}


/**
 * Returns the elements with the given id, using a pre-computed map.
 * @param {String} file The path of the Akoma Ntoso file in which execute the command
 * @param {String} id The value of the attribute eId to search
 * @param {Object} [options] The options for the function call
 * @param {String} [options.idName] The name of the attribute that contains the id (default is 'eId')
 * @return {Object} A list of XML element with the given Id, if they exist, else undefined
 */
const _getElementById = function(file, id, options) {
    let akomantoso = _openAkomando(file, options);
    if (akomantoso != null) {
        let retList = akomantoso.getElementById({ idName: options.idName, idValue: id });
        if(retList == undefined)
            retList = [];
        const retString = `found ${retList.length} element(s):\n\n` +
            retList.map((elem) => pd.xml(elem.toString()))
            .join('\n\n');
        console.log(retString);
        return retString;
    }
}


program
   .version('0.0.2');
program
   .command('get-doc-type <file>')
   .option('-c, --config <config>', 'the configuration file')
   .description('Returns the doc type of the AkomaNtoso document.')
   .action(_getDocType);
program
   .command('get-akomantoso <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('-s, --serialize <serialize>', 'the requested serialization. Supported serialization are akn, html, json and text')
   .option('--no-html', 'when html serialization is requested, specifies if the html element must be inserted in the html')
   .option('--no-head', 'when html serialization is requested, specifies if the head element must be inserted in the html')
   .option('--no-body', 'when html serialization is requested, specifies if the body element must be inserted in the html')
   .option('--no-new-lines', 'when text serialization is requested, specifies if the newl lines in the original document must preserved or not preserved')
   .description('Returns the loaded AkomaNtoso document serialized as xml, html or json')
   .action(_getAkomaNtoso);
program
   .command('get-meta <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('-s, --serialize <serialize>', 'the requested serialization. Supported serialization are akn, html and json')
   .option('-m, --meta-root <metaRoot>', 'the meta elment to retrieve')
   .option('--no-data-xpath', 'specifies if the data-akomando-xpath attribute must be inserted in the returned elements')
   .option('--no-html', 'when html serialization is requested, specifies if the html element must be inserted in the html')
   .option('--no-head', 'when html serialization is requested, specifies if the head element must be inserted in the html')
   .option('--no-body', 'when html serialization is requested, specifies if the body element must be inserted in the html')
   .description('Returns meta of the AkomaNtoso document.')
   .action(_getMeta);
program
   .command('get-components <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('-s, --serialize <serialize>', 'the requested serialization. Supported serialization are akn, html and json')
   .option('--no-detailed-info', 'if this parameter is set, the command returns an array containing just the components found in the document in the requested serialization')
   .option('--no-data-xpath', 'specifies if the data-akomando-xpath attribute must be inserted in the returned elements')
   .option('--no-html', 'when html serialization is requested, specifies if the html element must be inserted in the html')
   .option('--no-head', 'when html serialization is requested, specifies if the head element must be inserted in the html')
   .option('--no-body', 'when html serialization is requested, specifies if the body element must be inserted in the html')
   .description('Returns a string representation of a JSON object containing all the components contained in the AkomaNtoso document.')
   .action(_getComponents);
program
   .command('get-hier-identifiers <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('--sort-by <sort>', 'the requested sorting. Supported sorting are position, name, eId, wId, GUID, xpath, level. Default sorting is position')
   .option('--order <ordering>', 'the requested ordering. Supported ordering are descending and ascending. Default ordering is ascending')
   .option('--filter-by-name <name>', 'set it to return only the hierarchies having the given name')
   .option('--filter-by-content', 'returns only the hierarchy having content')
   .description('Returns a JSON string containing all the identifiers of hierarchies contained in the file')
   .action(_getHierIdentifiers);
program
   .command('count-doc-elements <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('--filter-by-name <name>', 'set it to return only the elements having the given name')
   .description('Returns a JSON string containing the total number of elements contained in the document and the total number of elements for each type')
   .action(_countDocElements);
program
   .command('get-references-info <file>')
   .option('-c, --config <config>', 'the configuration file')
   .option('--filter-by <filter>', 'the requested filtering. Supported filtering are: all (returns all the references), used (returns only references that are used by at last one element in the document), unused (returns only the references that are not used by any element in the document). Default filtering is all')
   .description('Returns a list of the references that are contained in the metadata section of the document. It also returns information about the references and information about the elements of the document that are linked to them.')
   .action(_getReferencesInfo);
program
    .command('get-id-references')
    .option('-c, --config <config>', 'the configuration file')
    .option('--filter-by <filter>', 'the requested filtering. Supported filtering are: all (default), existing (only references to an id that exist in the document), pending (only references to an id that doesn\'t exist in the document).')
    .description('Returns,for each id, a list of elements that refers to it.')
    .action(_getIdReferences);
program
    .command('get-elements-from-xpath <file> <xpath>')
    .option('-c, --config <config>', 'the configuration file')
    .description('Search reference element in the document about the href passed as parameter, if exist')
    .action(_getElementsFromXPath);
program
    .command('get-frbr <file>')
    .option('-c, --config <config>', 'the configuration file')
    .description('Returns an object containing the URIs of Work, Expression and Manifestation of the document')
    .action(_getFRBR);
program
    .command('get-publication-info <file>')
    .option('-c, --config <config>', 'the configuration file')
    .option('-a, --about <about>', 'Specifies what results we can receive. Possible values are: work, expression, manifestation', 'expression')
    .description('Returns author and publication/creation date information about Manifestation, Expression or Work of the document')
    //.action(() => Error("Not yet implemented"));
    .action(_getPublicationInfo);
program
    .command('check-ids <file>')
    .option('-c, --config <config>', 'the configuration file')
    .option('-n, --id-name <id-name>', 'The name of the attribute that represents the id (default: all)', 'all')
    .description('Check whether the ids in the document are well-formed')
    .action(_CheckIds);
program
    .command('get-element-by-id <file> <id>')
    .option('-c, --config <config>', 'the configuration file')
    .option('--id-name <id-name>', 'The name of the attribute represents the id. Default is \'eId\'', 'eId')
    .description('Returns the elements with the given id')
    .action(_getElementById);

program.parse(process.argv);
if (program.args.length === 0)
    program.help();
