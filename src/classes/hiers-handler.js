// libs
import _ from 'underscore';

// utils
import { _getDocumentHiers,
        _getHierIdentifierObject,
        } from '../constants/utils/utils.js';

/**
 * This Class is used to handle hierarchies in the AkomaNtoso documents.
*/
export default class _HiersHandler {

   /**
    * Creates an AkomaNtoso hiers handler
   */
   constructor(){
   }


   /**
    * An akomando.hier.identifier object containing all the identifiers of all partions
    * of the given document (according to the given akomand.config or to passed ones).
    * @param {Object} options The options for retrieving identifiers
    * @param {Object} [options.config] The configuration file according to {@link akomando.config}
    * @param {Object} [options.AKNDom] An AkomanNtoso as a XML DOM
    * @param {String} [options.docType] The type of the document contained in the AKNDom parameter
    * @param {Object} [options.prefix] The prefix that is used in the document for the elements (if any)
    * @param {String} [options.sortyBy=position] The field on wich results must be sorted. Possible values are:
    *
    * - position - The position of the element insiede the document
    * - name - The name of the element
    * - eId - The eId of the element
    * - wId - The wId of the element
    * - GUID - The GUID of the element
    * - xpath - The xpath of the element
    * - level - The level of the element
    *
    * @param {String} [options.order=ascending] The ordering of the returnet elements. Possible values are:
    *
    * - ascending - Results are ordered in ascending way
    * - descending - Results are ordered in descending way
    *
    * @param {String} [options.filterByName=null] Returns only the hierarchies having the given name
    * @param {Boolean} [options.filterByContent=false] Returns only the hierarchies having containing text (the last level partitions in hierarchies)
    * @return {Object} an {@link akomando.hier.identifier} object containing all the identifiers of all
    * hierarchies of the given document
   */
   static getHiersIdentifiers({
      config,
      AKNDom,
      docType,
      prefix,
      sortBy,
      order,
      filterByName,
      filterByContent,
    }={}){
      let docHierIds = [];
      if(docType === config.docTypes.documentCollection.name){
         const allDocTypes =
            _.chain(config.docTypes)
            .pluck('name')
            .map((el)=>{
               return (prefix) ? `${prefix}:${el}` : el;
            })
            .value();
         const components = AKNDom.getElementsByTagName(config.component.name);
         for (var c = 0; c < components.length; c++){
            const componentDoc = components[c].firstChild;
            if (_.contains(allDocTypes, componentDoc.nodeName)){
               const hiersForComponent = this._getHiersIndetifiersForDoc({
                  config,
                  AKNDom: componentDoc,
                  docType: componentDoc.nodeName,
                  prefix,
                  filterByContent,
               });
               docHierIds = docHierIds.concat(hiersForComponent);
            }
         }
      }
      else{
         docHierIds = this._getHiersIndetifiersForDoc({
            config,
            AKNDom,
            docType,
            prefix,
            filterByContent,
         });
      }
      if (docHierIds.length == 0){
         return [];
      }
      docHierIds = (()=>{
         if(filterByName){
            const nameToFilter = (prefix) ? `${prefix}:${filterByName}`: filterByName;
            return _.where(docHierIds, {name: nameToFilter});
         }
         return docHierIds;
      })();
      let sortedHiers = (sortBy==='position') ?
         docHierIds :
         _.sortBy(docHierIds, sortBy);
      return (order=='ascending') ? sortedHiers : sortedHiers.reverse();
   }

   /**
    * An akomando.hier.identifier object containing all the identifiers of all partions
    * of the given document (according to the given akomand.config or to passed ones).
    * @param {Object} options The options for retrieving identifiers
    * @param {Object} [options.config] The configuration file according to {@link akomando.config}
    * @param {Object} [options.AKNDom] An AkomanNtoso as a XML DOM
    * @param {Boolean} [options.filterByContent=false] Returns only the hierarchies having containing text (the last level partitions in hierarchies)
    * @return {Object} an akomando.hier.identifier object containing all the identifiers of all
    * hierarchies of the given document
   */
   static _getHiersIndetifiersForDoc({
      config,
      AKNDom,
      docType,
      prefix,
      filterByContent,
   }={}){
      const docTypeMainContainer = (prefix)?
         `${prefix}:${config.docTypes[docType].mainContainer.name}`:
         config.docTypes[docType].mainContainer.name;
      const hiersContentElement = (prefix)?
         `${prefix}:${config.docTypes[docType].hierarchies.contentElement.name}`:
         config.docTypes[docType].hierarchies.contentElement.name;

      const hierModElements =
         _.chain(config.docTypes[docType].hierarchies.modElements)
         .pluck('name')
         .map((el)=>{
            return (prefix) ? `${prefix}:${el}` : el;
         })
         .value();
      const hierBlockElements =
         _.chain(config.docTypes[docType].hierarchies.blockElements)
         .pluck('name')
         .map((el)=>{
            return (prefix) ? `${prefix}:${el}` : el;
         })
         .value();
      const hierQuotedStructureElement = (prefix)?
         `${prefix}:${config.docTypes[docType].hierarchies.quotedStructureElement.name}`:
         config.docTypes[docType].hierarchies.quotedStructureElement.name;
      const docTypeHiers =
         _.chain(config.docTypes[docType].hierarchies.partitions)
         .pluck('name')
         .map((el)=>{
            return (prefix) ? `${prefix}:${el}` : el;
         })
         .value()
         .concat(hierModElements)
         .concat(hierBlockElements)
         .concat(hierQuotedStructureElement)
         .concat(hiersContentElement);
      const hiers = _getDocumentHiers(
         docTypeMainContainer,
         hiersContentElement,
         hierBlockElements,
         AKNDom.getElementsByTagName(docTypeMainContainer),
         docTypeHiers);
      let docHierIds = [];
      for (var h=0; h < hiers.length; h++){
         if(filterByContent){
            let checkContinue = false;
            for (var c = 0; c < hiers[h].childNodes.length; c++){
               if(hiers[h].childNodes[c].nodeName == hiersContentElement){
                  checkContinue = true;
               }
            }
            if(!checkContinue){
               continue;
            }
         }

         docHierIds.push(_getHierIdentifierObject(hiers[h],docTypeMainContainer));
      }
      return docHierIds;
   }
}
