import _ from 'underscore';

// utils
import { _getElementsByTagNameUniversal,
         _serializeNodes,
         _getIdentifierObject,
         _getNodePosition,
         _getElementAttributes } from '../constants/utils/utils.js';


/**
* A class to handle all features related to components of AkomaNtoso documents.
* The class can be used for the following:
*
* - to request all the components contained in a document and put a reference to their DOM in a Object
* - to request single components contained in a document
*/
export default class _ComponentsHandler {
   /**
   * Create an AkomaNtoso components handler.
   */
   constructor(){
   }

   /**
     * Object schema for the akomando getComponents command
     * @typedef {Object} akomando.components.getComponents
     * @property {Object} components The main object
     * @property {Object} components.component Contains the information about a component
     * @property {Number} components.components.positionInWholeDocument The sequential number of the component in the whole document
     * @property {Number} components.components.positionInOwnerDocument The sequential number of the component in the owner document
     * @property {Boolean} components.components.isReferenceToComponent True if it is a reference to a component (i.e. if it contains just a documentRef or a componentRef element)
     * @property {Object} components.components.referencedComponentInfo if {@link components.components.isReferenceToComponent} is true, this contains information about the referenced component. Otherwise it is an empty object
     * @property {String} components.components.referencedComponentInfo.referenceName The tag name of the reference
     * @property {Boolean} components.components.referencedComponentInfo.isInternal True if it is an internal reference
     * @property {Boolean} components.components.referencedComponentInfo.referencedComponentIsUnique True if the referenced component is unique in the document
     * @property {Object} components.components.referencedComponentInfo.referencedComponentsInfo The information about the referenced component
     * @property {String} components.components.referencedComponentInfo.referencedComponentsInfo.name The tag name of the referenced component
     * @property {String} components.components.referencedComponentInfo.referencedComponentsInfo.eId The eId attribute of the referenced component
     * @property {String} components.components.referencedComponentInfo.referencedComponentsInfo.wId The wId attribute of the referenced component
     * @property {String} components.components.referencedComponentInfo.referencedComponentsInfo.GUID The GUID attribute of the referenced component
     * @property {String} components.components.referencedComponentInfo.referencedComponentsInfo.xpath The xpath attribute of the referenced component
     * @property {Object} components.components.referencedComponentInfo.referenceInfo The information about the reference contained in the component element
     * @property {String} components.components.referencedComponentInfo.referenceInfo.name The tag name of the reference contained in the component element
     * @property {String} components.components.referencedComponentInfo.referenceInfo.eId The eId attribute of the reference contained in the component element
     * @property {String} components.components.referencedComponentInfo.referenceInfo.wId The wId attribute of the reference contained in the component element
     * @property {String} components.components.referencedComponentInfo.referenceInfo.GUID The GUID attribute of the reference contained in the component element
     * @property {String} components.components.referencedComponentInfo.referenceInfo.xpath The xpath attribute of the reference contained in the component element
     * @property {Array} components.components.referencedComponentInfo.referenceAttributes The list of the attributes of the reference contained in the component element
     * @property {String} components.components.referencedComponentInfo.referenceAttributes.name The name of the attribute
     * @property {String} components.components.referencedComponentInfo.referenceAttributes.value The value of the attribute
     * @property {Boolean} components.components.isReferencedInternally If it is not a reference to a component and, so, if the component contains a whole document, this is true if the component is referenced internally somewhere in the document
     * @property {Array} components.components.internalReferralsInfo If {@link components.components.isReferencedInternally} is true, this contains a list  of internal referrals and their related information. Otherwise it is an empty list
     * @property {String} components.components.internalReferralsInfo.name The tag name of the internal referral
     * @property {String} components.components.internalReferralsInfo.eId The eId attribute of the internal referrals
     * @property {String} components.components.internalReferralsInfo.wId The eId attribute of the internal referrals
     * @property {String} components.components.internalReferralsInfo.GUID The eId attribute of the internal referrals
     * @property {String} components.components.internalReferralsInfo.xpath The eId attribute of the internal referral
     * @property {Object} components.components.ownerInfo The information about the owner document of the component
     * @property {String} components.components.ownerInfo.name The tag name of the owner document
     * @property {String} components.components.ownerInfo.eId The eId attribute of the owner document
     * @property {String} components.components.ownerInfo.wId The wId attribute the owner document
     * @property {String} components.components.ownerInfo.GUID The GUID attribute the owner document
     * @property {String} components.components.ownerInfo.xpath The xpath attribute the owner document
     * @property {Array} components.components.componentAttributesList The list of the attributes of the component
     * @property {String} components.components.componentAttributesList.name The name of the attribute
     * @property {String} components.components.componentAttributesList.value The value of the attribute
     * @property {Object|String|Node} components.components.componentDocument The actual component document in a one of the following supported serialization (AKNDom, AKNString, HTMLDom, HTMLString, JSON, JSONString)
     *
     * @example
     * {
     *     "components": [
     *         {
     *             "component": {
     *                 "positionInWholeDocument": 1,
     *                 "positionInOwnerDocument": 1,
     *                 "isReferenceToComponent": true,
     *                 "referencedComponentInfo": {
     *                     "referenceName": "documentRef",
     *                     "isInternal": true,
     *                     "referencedComponentIsUnique": true,
     *                     "referencedComponentsInfo": [
     *                         {
     *                             "name": "component",
     *                             "eId": "cmpnts__cmp_1",
     *                             "wId": "doc1",
     *                             "GUID": "",
     *                             "xpath": "/akomaNtoso[1]/components[1]/component[1]"
     *                         }
     *                     ],
     *                     "referenceInfo": {
     *                         "name": "documentRef",
     *                         "eId": "dref_1",
     *                         "wId": "ref_doc1",
     *                         "GUID": "",
     *                         "xpath": "/akomaNtoso[1]/documentCollection[1]/collectionBody[1]/component[1]/documentRef[1]"
     *                     },
     *                     "referenceAttributes": [
     *                         {
     *                             "name": "showAs",
     *                             "value": "document 1"
     *                         },
     *                         {
     *                             "name": "eId",
     *                             "value": "dref_1"
     *                         },
     *                         {
     *                             "name": "href",
     *                             "value": "#doc1"
     *                         },
     *                         {
     *                             "name": "wId",
     *                             "value": "ref_doc1"
     *                         }
     *                     ]
     *                 },
     *                 "isReferencedInternally": false,
     *                 "internalReferralsInfo": [],
     *                 "ownerInfo": {
     *                     "name": "documentCollection",
     *                     "eId": "",
     *                     "wId": "",
     *                     "GUID": "",
     *                     "xpath": "/akomaNtoso[1]/documentCollection[1]"
     *                 },
     *                 "componentInfo": {
     *                     "name": "component",
     *                     "eId": "cmp_1",
     *                     "wId": "comp1",
     *                     "GUID": "",
     *                     "xpath": "/akomaNtoso[1]/documentCollection[1]/collectionBody[1]/component[1]"
     *                 },
     *                 "componentAttributesList": [
     *                     {
     *                         "name": "eId",
     *                         "value": "cmp_1"
     *                     },
     *                     {
     *                         "name": "wId",
     *                         "value": "comp1"
     *                     }
     *                 ],
     *                 "componentDocument": "... the document in the requested serialization ... "
     *             }
     *         },
     *         {
     *             "component": {
     *                 "positionInWholeDocument": 7,
     *                 "positionInOwnerDocument": 1,
     *                 "isReferenceToComponent": false,
     *                 "referencedComponentInfo": {},
     *                 "isReferencedInternally": true,
     *                 "internalReferralsInfo": [
     *                     {
     *                         "name": "documentRef",
     *                         "eId": "dref_1",
     *                         "wId": "ref_doc1",
     *                         "GUID": "",
     *                         "xpath": "/akomaNtoso[1]/documentCollection[1]/collectionBody[1]/component[1]/documentRef[1]"
     *                     }
     *                 ],
     *                 "ownerInfo": {
     *                     "name": "akomaNtoso",
     *                     "eId": "",
     *                     "wId": "",
     *                     "GUID": "",
     *                     "xpath": "/akomaNtoso[1]"
     *                 },
     *                 "componentInfo": {
     *                     "name": "component",
     *                     "eId": "cmpnts__cmp_1",
     *                     "wId": "doc1",
     *                     "GUID": "",
     *                     "xpath": "/akomaNtoso[1]/components[1]/component[1]"
     *                 },
     *                 "componentAttributesList": [
     *                     {
     *                         "name": "eId",
     *                         "value": "cmpnts__cmp_1"
     *                     },
     *                     {
     *                         "name": "wId",
     *                         "value": "doc1"
     *                     }
     *                 ],
     *                 "componentDocument": "... the document in the requested serialization ... "
     *             }
     *         },
     *     ]
     * }
     *
   */

   /**
     * Returns the components contained in the document in an object structured as {@link akomando.components.getComponents}
     * @param {Object} options Options for the request object and the found components
     * @param {String} [options.serialize=AKNDom] - The serialization of the returning components. Possible values are:
     * - AKNDom - returns the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     * - AKNDom - returns the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     *     Document</a> serialization of the components contained in the document
     * - AKNString - returns the String serialization of the components contained in the document
     * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     *     Document</a> serialization of the components contained in the document
     * - HTMLString - returns the HTML string serialization of the components contained in the document
     * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
     *     JSON</a> serialization of the components contained in the document
     * - JSONString - returns the JSON string serialization of the components contained in the document
     * @param {String} [options.namespace=null] - The namespace in which components must be searched
     * @param {String} [options.prefix=null] - The prefix of the compenents that must be found
     * @param {Document} [options.AKNDom=null] - The XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     * @param {String} [options.insertDataXPath=false] - Set it to true to add the data-akomando-xpath attribute to each of the returned elements
     * @param {String} [options.detailedInfo=true] - If it is set to false, the function returns only an array containing the components in the requested serialization
     * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document (if the HTML serialization is required)
     * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document (if the HTML serialization is required)
     * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document (if the HTML serialization is required)
     * Document</a> serialization of the AkomaNtoso in which the research must be performed
     * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
     * @return {Object} An object structured as a {@link akomando.components.getComponents} object and containing all compoments in the requested serialization
   */
   static getComponents({
      serialize = 'AKNDom',
      namespace = null,
      AKNDom = null,
      detailedInfo,
      insertDataXPath,
      addHtmlElement,
      addHeadElement,
      addBodyElement,
      config,
   } = {}){
      let allComponents = [
         ... _getElementsByTagNameUniversal(
            AKNDom.documentElement,
            config.component.name,
            namespace
      )];

      const results = {
         [`${config.components.name}`]: [],
      };

      const referralElements =
         _(config.component.referencedIn)
         .chain()
         .where({type:'element'})
         .pluck('name')
         .value();

      allComponents.forEach( (componentDOM, i) => {
         let componentSerialized = _serializeNodes(
            [componentDOM],
            serialize, {
               insertDataXPath,
               addHtmlElement,
               addHeadElement,
               addBodyElement,
            }
         )[0];
         if(!detailedInfo) {
            return results.components.push(componentSerialized);
         }

         let componentAttributesList = _getElementAttributes(componentDOM);
         let componentInfo = _getIdentifierObject(componentDOM);
         let componentIdentifiersAttributes =   _(componentAttributesList)
                                                .chain()
                                                .filter( el => (
                                                   el.name === 'eId' ||
                                                   el.name === 'wId' ||
                                                   el.name === 'GUID'
                                                ))
                                                .pluck('value')
                                                .map(el=>(`#${el}`))
                                                .value();
         let isReferenceToComponent = referralElements.includes(componentDOM.firstChild.nodeName);
         let internalReferrals = [];
         referralElements.forEach( el => {
            let referrals = [... _getElementsByTagNameUniversal(
               AKNDom.documentElement,
               el,
               namespace
            )].filter( referral => (
               componentIdentifiersAttributes.includes(
                  _(_getElementAttributes(referral))
                  .chain()
                  .where({name:'href'})
                  .pluck('value')
                  .value()[0]
               )
            ));
            if(referrals.length > 0){
               internalReferrals.push(referrals);
            }
         });
         let isReferencedInternally = (internalReferrals.length !== 0);
         let internalReferralsInfo = internalReferrals.map( referral => (
            _getIdentifierObject(referral[0])
         ));
         let referencedComponentInfo =
            (isReferenceToComponent) ?
               (() => {
                  let referenceName = componentDOM.firstChild.nodeName;
                  let referenceAttributes = _getElementAttributes(componentDOM.firstChild);
                  let referenceInfo = _getIdentifierObject(componentDOM.firstChild);
                  let referenceHrefAttribute = _(referenceAttributes)
                                                .chain()
                                                .where({name:'href'})
                                                .pluck('value')
                                                .value()[0];
                  let isInternal = referenceHrefAttribute.startsWith('#');
                  let referencedComponents = (isInternal) ?
                     allComponents.filter( component => {
                        let componentIdentifiersAttributes =
                           _(_getElementAttributes(component))
                           .chain()
                           .filter( el => (
                              el.name === 'eId' ||
                              el.name === 'wId' ||
                              el.name === 'GUID'
                           ))
                           .pluck('value')
                           .map(el=>(`#${el}`))
                           .value();
                        return componentIdentifiersAttributes.includes(referenceHrefAttribute);
                     }) : [];
                  let referencedComponentIsUnique = (referencedComponents.length === 1);
                  let referencedComponentsInfo = referencedComponents.map( refComp => (
                     _getIdentifierObject(refComp)
                  ));
                  return {
                     referenceName,
                     isInternal,
                     referencedComponentIsUnique,
                     referencedComponentsInfo,
                     referenceInfo,
                     referenceAttributes,
                  };
               })() : {};

         results.components.push({
            [`${config.component.name}`]: {
               positionInWholeDocument: i + 1,
               positionInOwnerDocument: _getNodePosition(componentDOM),
               isReferenceToComponent,
               referencedComponentInfo,
               isReferencedInternally,
               internalReferralsInfo,
               ownerInfo: _getIdentifierObject(componentDOM.parentNode.parentNode),
               componentInfo,
               componentAttributesList,
               componentDocument: componentSerialized,
            },
         });
      });
      return [results];
   }
}
